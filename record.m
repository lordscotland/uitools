#import <AVFoundation/AVFoundation.h>
#import <CoreVideo/CVPixelBuffer.h>
#import <dispatch/dispatch.h>
#include <mach/mach_time.h>
#include "capture.h"

#define IGNORE_MSG "W: Ignoring option -%c\n"

static void handle_interrupt(int sig) {
  static BOOL stopped=NO;
  if(stopped){exit(1);}
  fputs("Terminating...\n",stderr);
  stopped=YES;
  CFRunLoopStop(CFRunLoopGetMain());
}
int main(int argc,char** argv) {
  int opt_mono=0;
  int opt_srate=0;
  float srate;
  char* audiofn=NULL;
  char* videofn=NULL;
  int opt;
  while((opt=getopt(argc,argv,"O:mr:o:"))!=-1){
    if(opt=='m'){opt_mono=opt;}
    else if(!optarg){return 1;}
    else if(opt=='O'){audiofn=optarg;}
    else if(opt=='o'){videofn=optarg;}
    else if(opt=='r'){
      opt_srate=opt;
      if(sscanf(optarg,"%f",&srate)!=1 || srate<=0){
        fprintf(stderr,"E[-%c]: Invalid argument\n",opt);
        return 1;
      }
    }
    else {fprintf(stderr,IGNORE_MSG,opt);}
  }
  if(!audiofn && !videofn){
    fprintf(stderr,"Usage: %s [-O <output.caf> [-m(ono)] [-r <sample_rate>]] [-o <output.mp4>]\n",argv[0]);
    return 1;
  }
  if(signal(SIGINT,handle_interrupt)==SIG_ERR){
    fputs("Error: Cannot handle SIGINT\n",stderr);
    return -1;
  }
  NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
  AVAudioRecorder* recorder;
  if(audiofn){
    if(access(audiofn,F_OK)!=-1){
      fprintf(stderr,"E: '%s' exists\n",audiofn);
      return 1;
    }
    AVAudioSession* session=[AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:NULL];
    [session setActive:YES error:NULL];
    CFURLRef URL=CFURLCreateFromFileSystemRepresentation(NULL,
     (const UInt8*)audiofn,strlen(audiofn),false);
    NSError* error;
    recorder=[[AVAudioRecorder alloc] initWithURL:(NSURL*)URL
     settings:[NSDictionary dictionaryWithObjectsAndKeys:
     [NSNumber numberWithInt:opt_mono?1:2],AVNumberOfChannelsKey,
     [NSNumber numberWithFloat:opt_srate?srate:44100],AVSampleRateKey,nil] error:&error];
    if(!recorder){
      fprintf(stderr,"E[AVAudioRecorder]: %s\n",
       error.localizedDescription.UTF8String);
      return 1;
    }
    CFRelease(URL);
    [recorder record];
  }
  else {
    if(opt_mono){fprintf(stderr,IGNORE_MSG,opt_mono);}
    if(opt_srate){fprintf(stderr,IGNORE_MSG,opt_srate);}
  }
  AVAssetWriter* videoWriter;
  AVAssetWriterInput* videoInput;
  AVAssetWriterInputPixelBufferAdaptor* videoAdaptor;
  IOSurfaceRef surface;
  if(videofn){
    if(access(videofn,F_OK)!=-1){
      fprintf(stderr,"E: '%s' exists\n",videofn);
      return 1;
    }
    CFURLRef URL=CFURLCreateFromFileSystemRepresentation(NULL,
     (const UInt8*)videofn,strlen(videofn),false);
    NSError* error;
    videoWriter=[[AVAssetWriter alloc] initWithURL:(NSURL*)URL
     fileType:AVFileTypeMPEG4 error:&error];
    if(!videoWriter){
      fprintf(stderr,"E[AVAssetWriter]: %s\n",
       error.localizedDescription.UTF8String);
      return 1;
    }
    CFRelease(URL);
    IOReturn status;
    IOMobileFramebufferRef iomfb;
    if((status=IOMobileFramebufferGetMainDisplay(&iomfb))){
      fprintf(stderr,"E[IOMobileFramebufferGetMainDisplay]: %d\n",status);
      return 1;
    }
    CGSize size;
    if((status=IOMobileFramebufferGetDisplaySize(iomfb,&size))){
      fprintf(stderr,"E[IOMobileFramebufferGetDisplaySize]: %d\n",status);
      return 1;
    }
    int sW=size.width,sH=size.height,vW=sW/16*16,vH=sH/16*16;
    int sX=(vW<sW)?((vW+=16)-sW)/2:0,sY=(vH<sH)?((vH+=16)-sH)/2:0;
    printf("Dimensions: %dx%d%+d%+d => %dx%d\n",sW,sH,sX,sY,vW,vH);
    NSNumber* format=[NSNumber numberWithInt:kCVPixelFormatType_32BGRA];
    NSNumber* width=[NSNumber numberWithInt:vW];
    NSNumber* height=[NSNumber numberWithInt:vH];
    videoInput=[[AVAssetWriterInput alloc]
     initWithMediaType:AVMediaTypeVideo outputSettings:[NSDictionary
     dictionaryWithObjectsAndKeys:AVVideoCodecH264,AVVideoCodecKey,
     width,AVVideoWidthKey,height,AVVideoHeightKey,nil]];
    videoInput.expectsMediaDataInRealTime=YES;
    [videoWriter addInput:videoInput];
    videoAdaptor=[[AVAssetWriterInputPixelBufferAdaptor alloc]
     initWithAssetWriterInput:videoInput sourcePixelBufferAttributes:[NSDictionary
     dictionaryWithObject:format forKey:(id)kCVPixelBufferPixelFormatTypeKey]];
    const int bpe=4,bpr=bpe*vW,btot=bpr*vH;
    CFDictionaryRef attrs=CFDictionaryCreate(NULL,
     (CFTypeRef[]){kIOSurfacePixelFormat,kIOSurfaceWidth,kIOSurfaceHeight,
     kIOSurfaceBytesPerElement,kIOSurfaceBytesPerRow,kIOSurfaceAllocSize,kIOSurfaceIsGlobal},
     (CFTypeRef[]){format,width,height,[NSNumber numberWithInt:bpe],
     [NSNumber numberWithInt:bpr],[NSNumber numberWithInt:btot],kCFBooleanTrue},7,NULL,NULL);
    surface=IOSurfaceCreate(attrs);
    CFRelease(attrs);
    void* surfaceptr=IOSurfaceGetBaseAddress(surface);
    [videoWriter startWriting];
    [videoWriter startSessionAtSourceTime:kCMTimeZero];
    CVPixelBufferPoolRef pbpool=videoAdaptor.pixelBufferPool;
    assert(pbpool);
    mach_timebase_info_data_t timebase;
    mach_timebase_info(&timebase);
    [videoInput requestMediaDataWhenReadyOnQueue:dispatch_get_main_queue() usingBlock:^{
      IOSurfaceLock(surface,0,NULL);
      CARenderServerRenderDisplay(0,CFSTR("LCD"),surface,-sX,-sY);
      static uint64_t tstart=0;
      uint64_t tdiff=mach_absolute_time();
      if(tstart){tdiff-=tstart;}
      else {tstart=tdiff;tdiff=0;}
      CVPixelBufferRef buffer;
      CVReturn status=CVPixelBufferPoolCreatePixelBuffer(NULL,pbpool,&buffer);
      if(status==kCVReturnSuccess){
        CVPixelBufferLockBaseAddress(buffer,0);
        memcpy(CVPixelBufferGetBaseAddress(buffer),surfaceptr,btot);
        assert([videoAdaptor appendPixelBuffer:buffer withPresentationTime:
         CMTimeMake(tdiff*timebase.numer/NSEC_PER_USEC,timebase.denom*USEC_PER_SEC)]);
        CVPixelBufferUnlockBaseAddress(buffer,0);
        CVPixelBufferRelease(buffer);
      }
      else {fprintf(stderr,"E[CVPixelBufferPoolCreatePixelBuffer]: %d\n",status);}
      IOSurfaceUnlock(surface,0,NULL);
    }];
  }
  fputs("Recording...\n",stderr);
  CFRunLoopRun();
  if(audiofn){
    [recorder stop];
    [recorder release];
  }
  if(videofn){
    CFRelease(surface);
    [videoInput markAsFinished];
    if([videoWriter respondsToSelector:
     @selector(finishWritingWithCompletionHandler:)]){
      [videoWriter finishWritingWithCompletionHandler:
       ^{CFRunLoopStop(CFRunLoopGetMain());}];
      CFRunLoopRun();
    }
    else {[videoWriter finishWriting];}
    if(videoWriter.status!=AVAssetWriterStatusCompleted){
      fprintf(stderr,"E[finishWriting]: %ld %s\n",(long)videoWriter.status,
       videoWriter.error.localizedDescription.UTF8String);
      return 1;
    }
    [videoAdaptor release];
    [videoInput release];
    [videoWriter release];
  }
  [pool drain];
}
