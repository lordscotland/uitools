#import <CoreFoundation/CoreFoundation.h>
#include <stdio.h>

typedef struct __WiFiNetwork *WiFiNetworkRef;
CFDataRef WiFiNetworkGetSSIDData(WiFiNetworkRef network);
CFDataRef WiFiNetworkCopyBSSIDData(WiFiNetworkRef network);
CFDateRef WiFiNetworkGetAssociationDate(WiFiNetworkRef network);
int WiFiNetworkGetAuthFlags(WiFiNetworkRef network);
Boolean WiFiNetworkIsHidden(WiFiNetworkRef network);
Boolean WiFiNetworkRequiresUsername(WiFiNetworkRef network);
Boolean WiFiNetworkRequiresPassword(WiFiNetworkRef network);

typedef struct __WiFiManager *WiFiManagerRef;
WiFiManagerRef WiFiManagerClientCreate(CFAllocatorRef allocator,int flags);
CFArrayRef WiFiManagerClientCopyNetworks(WiFiManagerRef client);
void WiFiManagerClientRemoveNetwork(WiFiManagerRef client,WiFiNetworkRef network);

int main(int argc,char** argv) {
  WiFiManagerRef client=WiFiManagerClientCreate(NULL,0);
  CFArrayRef networks=WiFiManagerClientCopyNetworks(client);
  CFIndex count=CFArrayGetCount(networks),n;
  WiFiNetworkRef* network=malloc(count*sizeof(WiFiNetworkRef));
  CFArrayGetValues(networks,CFRangeMake(0,count),(const void**)network);
  for (n=0;n<count;n++){
    putchar(WiFiNetworkIsHidden(network[n])?'x':'-');
    putchar(WiFiNetworkRequiresUsername(network[n])?'u':'-');
    putchar(WiFiNetworkRequiresPassword(network[n])?'p':'-');
    CFDataRef bssid=WiFiNetworkCopyBSSIDData(network[n]);
    if(bssid){
      const UInt8* bssid_ptr=CFDataGetBytePtr(bssid);
      CFIndex bssid_len=CFDataGetLength(bssid),i;
      for (i=0;i<bssid_len;i++){printf("%c%02x",i?':':' ',bssid_ptr[i]);}
      CFRelease(bssid);
    }
    else {fputs(" -----------------",stdout);}
    time_t assoc_time=kCFAbsoluteTimeIntervalSince1970
     +CFDateGetAbsoluteTime(WiFiNetworkGetAssociationDate(network[n]));
    char tbuf[18];
    strftime(tbuf,sizeof(tbuf),"%Y-%m-%d %H:%M",localtime(&assoc_time));
    printf(" %04d %s ",WiFiNetworkGetAuthFlags(network[n]),tbuf);
    CFDataRef ssid=WiFiNetworkGetSSIDData(network[n]);
    if(ssid){fwrite(CFDataGetBytePtr(ssid),1,CFDataGetLength(ssid),stdout);}
    printf(" [%ld]\n",n);
  }
  while(scanf("%ld",&n)==1){
    if(network[n]){
      printf("Removing [%ld]...\n",n);
      WiFiManagerClientRemoveNetwork(client,network[n]);
      network[n]=NULL;
    }
  }
  free(network);
  CFRelease(networks);
  CFRelease(client);
}
