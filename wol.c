#include <arpa/inet.h>
#include <netdb.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

int main(int argc,char** argv) {
  if(argc<2){
    fprintf(stderr,"Usage: %s <HWaddr> [<host>][:<port>]\n",argv[0]);
    return 1;
  }
  const char* hexchars="0123456789ABCDEFabcdef";
  unsigned char hwaddr[6];
  char* ptr=argv[1];
  int i;
  bool delim=true;
  for (i=0;i<sizeof(hwaddr);i++){
    int len=2;
    if(delim){
      if(i){ptr+=strcspn(ptr,hexchars);}
      int vlen=strspn(ptr,hexchars);
      if(vlen<len){len=vlen;}
      else if(i==0 && ptr[vlen]==0){delim=false;}
      else {ptr+=vlen-len;}
    }
    if(sscanf(ptr,"%2hhx",&hwaddr[i])!=1){
      fprintf(stderr,"E[&HWaddr]: Expecting hex digits, got '%s'\n",ptr);
      return 1;
    }
    ptr+=len;
  }
  if(*ptr){fprintf(stderr,"W[&HWaddr]: Ignoring extra bytes '%s'\n",ptr);}
  printf("HWaddr= %02X:%02X:%02X:%02X:%02X:%02X\n",
   hwaddr[0],hwaddr[1],hwaddr[2],hwaddr[3],hwaddr[4],hwaddr[5]);
  char* hoststr=(2<argc)?argv[2]:NULL;
  unsigned short port;
  if(hoststr && (ptr=strchr(hoststr,':'))){
    *(ptr++)=0;
    if(sscanf(ptr,"%hu",&port)!=1){
      fprintf(stderr,"E[&port]: Expecting decimal digits, got '%s'\n",ptr);
      return 1;
    }
  }
  else {port=9;}
  int fd=socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
  if(fd<0){
    perror("E[socket]");
    return -1;
  }
  if(setsockopt(fd,SOL_SOCKET,SO_BROADCAST,(int[]){true},sizeof(int))<0){
    perror("E[setsockopt]");
    return -1;
  }
  struct sockaddr_in addr={.sin_family=AF_INET,.sin_port=htons(port)};
  if(hoststr && *hoststr){
    struct hostent* he=gethostbyname(hoststr);
    if(!he){
      herror("E[gethostbyname]");
      return -1;
    }
    struct in_addr** list=(struct in_addr**)he->h_addr_list;
    if(!list || !list[0]){
      fprintf(stderr,"E[gethostbyname]: Cannot resolve '%s'\n",hoststr);
      return -1;
    }
    addr.sin_addr=*list[0];
  }
  else {addr.sin_addr.s_addr=INADDR_BROADCAST;}
  printf("IPaddr= %s : %hu\n",inet_ntoa(addr.sin_addr),port);
  unsigned char message[102]={0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
  ptr=(char*)message;
  for (i=0;i<16;i++){memcpy(ptr+=6,hwaddr,6);}
  if(sendto(fd,(char*)message,sizeof(message),0,(struct sockaddr*)&addr,sizeof(addr))<0){
    perror("E[sendto]");
    return -1;
  }
  close(fd);
}
