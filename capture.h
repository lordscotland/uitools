typedef kern_return_t IOReturn;
typedef CFTypeRef IOMobileFramebufferRef;
typedef CFTypeRef IOSurfaceRef;
extern const CFStringRef kIOSurfaceAllocSize;
extern const CFStringRef kIOSurfaceBytesPerElement;
extern const CFStringRef kIOSurfaceBytesPerRow;
extern const CFStringRef kIOSurfaceHeight;
extern const CFStringRef kIOSurfaceIsGlobal;
extern const CFStringRef kIOSurfacePixelFormat;
extern const CFStringRef kIOSurfaceWidth;

extern IOReturn IOMobileFramebufferGetMainDisplay(IOMobileFramebufferRef*);
extern IOReturn IOMobileFramebufferGetDisplaySize(IOMobileFramebufferRef,CGSize*);
extern IOSurfaceRef IOSurfaceCreate(CFDictionaryRef);
extern IOReturn IOSurfaceLock(IOSurfaceRef,uint32_t,uint32_t*);
extern IOReturn IOSurfaceUnlock(IOSurfaceRef,uint32_t,uint32_t*);
extern void* IOSurfaceGetBaseAddress(IOSurfaceRef);
extern void CARenderServerRenderDisplay(kern_return_t,CFStringRef,IOSurfaceRef,int,int);
