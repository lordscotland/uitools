#import <CoreFoundation/CoreFoundation.h>

typedef CFTypeRef SCPreferencesRef;
typedef CFTypeRef SCNetworkSetRef;
typedef CFTypeRef SCNetworkServiceRef;
typedef CFTypeRef SCNetworkProtocolRef;

extern CFStringRef kSCNetworkProtocolTypeProxies;
extern CFStringRef kSCPropNetProxiesSOCKSProxy;
extern CFStringRef kSCPropNetProxiesSOCKSPort;
extern CFStringRef kSCPropNetProxiesSOCKSEnable;

extern CFDictionaryRef SCDynamicStoreCopyProxies(CFTypeRef);
extern SCPreferencesRef SCPreferencesCreate(CFAllocatorRef,CFStringRef,CFStringRef);
extern Boolean SCPreferencesLock(SCPreferencesRef,Boolean);
extern Boolean SCPreferencesUnlock(SCPreferencesRef);
extern Boolean SCPreferencesApplyChanges(SCPreferencesRef);
extern Boolean SCPreferencesCommitChanges(SCPreferencesRef);
extern SCNetworkSetRef SCNetworkSetCopyCurrent(SCPreferencesRef);
extern CFArrayRef SCNetworkSetCopyServices(SCNetworkSetRef);
extern CFStringRef SCNetworkServiceGetName(SCNetworkServiceRef);
extern SCNetworkProtocolRef SCNetworkServiceCopyProtocol(SCNetworkServiceRef,CFStringRef);
extern CFDictionaryRef SCNetworkProtocolGetConfiguration(SCNetworkProtocolRef);
extern Boolean SCNetworkProtocolSetConfiguration(SCNetworkProtocolRef,CFDictionaryRef);

static void fputCFString(CFStringRef string,FILE* fh) {
  CFIndex pos=0,remain=CFStringGetLength(string);
  while(remain>0){
    UInt8 bytes[4096];
    CFIndex nbytes,nconv=CFStringGetBytes(string,CFRangeMake(pos,remain),
     kCFStringEncodingUTF8,'?',false,bytes,sizeof(bytes),&nbytes);
    fwrite(bytes,1,nbytes,fh);
    pos+=nconv;
    remain-=nconv;
  }
}

int main(int argc,char** argv) {
  if(1<argc){
    char* proxy=argv[1];
    char* ptr=strrchr(proxy,':');
    if(ptr){*ptr++=0;}
    else {
      ptr=proxy;
      proxy="localhost";
    }
    int port;
    Boolean enable=sscanf(ptr,"%d",&port)==1;
    CFStringRef cfProxy;
    CFNumberRef cfPort,cfEnable;
    if(enable){
      cfEnable=CFNumberCreate(NULL,kCFNumberSInt32Type,(SInt32[]){true});
      cfProxy=CFStringCreateWithCString(NULL,proxy,kCFStringEncodingUTF8);
      cfPort=CFNumberCreate(NULL,kCFNumberIntType,&port);
    }
    const char* errmsg=NULL;
    SCPreferencesRef prefs=SCPreferencesCreate(NULL,CFSTR("com.officialscheduler.uitools.socks"),NULL);
    if(SCPreferencesLock(prefs,true)){
      SCNetworkSetRef netSet=SCNetworkSetCopyCurrent(prefs);
      CFArrayRef netServices=SCNetworkSetCopyServices(netSet);
      CFIndex count=CFArrayGetCount(netServices),i;
      for (i=0;i<count;i++){
        SCNetworkServiceRef service=CFArrayGetValueAtIndex(netServices,i);
        SCNetworkProtocolRef protocol=SCNetworkServiceCopyProtocol(service,kSCNetworkProtocolTypeProxies);
        if(protocol){
          CFMutableDictionaryRef config=CFDictionaryCreateMutableCopy(NULL,
           0,SCNetworkProtocolGetConfiguration(protocol));
          if(enable){
            CFDictionarySetValue(config,kSCPropNetProxiesSOCKSEnable,cfEnable);
            CFDictionarySetValue(config,kSCPropNetProxiesSOCKSProxy,cfProxy);
            CFDictionarySetValue(config,kSCPropNetProxiesSOCKSPort,cfPort);
          }
          else {
            CFDictionaryRemoveValue(config,kSCPropNetProxiesSOCKSEnable);
            CFDictionaryRemoveValue(config,kSCPropNetProxiesSOCKSProxy);
            CFDictionaryRemoveValue(config,kSCPropNetProxiesSOCKSPort);
          }
          if(SCNetworkProtocolSetConfiguration(protocol,config)){
            fputs(enable?"(+) ":"(-) ",stdout);
            fputCFString(SCNetworkServiceGetName(service),stdout);
            fputc('\n',stdout);
          }
          CFRelease(config);
          CFRelease(protocol);
        }
      }
      CFRelease(netServices);
      CFRelease(netSet);
      if(!SCPreferencesCommitChanges(prefs)){errmsg="E: CommitChanges\n";}
      else if(!SCPreferencesApplyChanges(prefs)){errmsg="E: ApplyChanges\n";}
      SCPreferencesUnlock(prefs);
    }
    else {errmsg="E: Lock\n";}
    CFRelease(prefs);
    if(errmsg){
      fputs(errmsg,stderr);
      return 1;
    }
    return 0;
  }
  int value;
  CFNumberRef cfnum;
  CFDictionaryRef config=SCDynamicStoreCopyProxies(NULL);
  if((cfnum=CFDictionaryGetValue(config,kSCPropNetProxiesSOCKSEnable))
   && CFNumberGetValue(cfnum,kCFNumberIntType,&value)){printf("SOCKSEnable: %d\n",value);}
  CFStringRef target=CFDictionaryGetValue(config,kSCPropNetProxiesSOCKSProxy);
  if(target){
    fputs("SOCKSProxy: ",stdout);
    fputCFString(target,stdout);
    fputc('\n',stdout);
  }
  if((cfnum=CFDictionaryGetValue(config,kSCPropNetProxiesSOCKSPort))
   && CFNumberGetValue(cfnum,kCFNumberIntType,&value)){printf("SOCKSPort: %d\n",value);}
  CFRelease(config);
}
