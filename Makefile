ARCHS = armv7 arm64
TARGET = iphone:latest:5.0

BINPATH = /usr/local/bin

include theos/makefiles/common.mk

MINIZ_OBJ := $(THEOS_OBJ_DIR)/miniz.o
$(MINIZ_OBJ): miniz.c; $(TARGET_CC) $(ALL_CFLAGS) -fvisibility=hidden -c -o $@ $<

TOOL_NAME += capture
capture_FILES = capture.c
capture_FRAMEWORKS = CoreFoundation CoreGraphics ImageIO MobileCoreServices QuartzCore
capture_PRIVATE_FRAMEWORKS = IOMobileFramebuffer IOSurface
capture_CODESIGN_FLAGS = -Scapture.xml
capture_INSTALL_PATH = $(BINPATH)

TOOL_NAME += ioservice
ioservice_FILES = ioservice.c
ioservice_FRAMEWORKS = CoreFoundation IOKit
ioservice_INSTALL_PATH = $(BINPATH)

TOOL_NAME += ipainstall
ipainstall_FILES = ipainstall.m
ipainstall_OBJ_FILES = $(MINIZ_OBJ)
ipainstall_FRAMEWORKS = MobileCoreServices
ipainstall_CODESIGN_FLAGS = -Sipainstall.xml
ipainstall_INSTALL_PATH = $(BINPATH)

TOOL_NAME += mcprofile
mcprofile_FILES = mcprofile.m
mcprofile_PRIVATE_FRAMEWORKS = ManagedConfiguration
mcprofile_CODESIGN_FLAGS = -Smcprofile.xml
mcprofile_INSTALL_PATH = $(BINPATH)

TOOL_NAME += open
open_FILES = open.m
open_FRAMEWORKS = MobileCoreServices
open_PRIVATE_FRAMEWORKS = SpringBoardServices
open_CODESIGN_FLAGS = -Sopen.xml
open_INSTALL_PATH = $(BINPATH)

TOOL_NAME += pbcopy
pbcopy_FILES = pbcopy.m
pbcopy_FRAMEWORKS = UIKit
pbcopy_INSTALL_PATH = $(BINPATH)

TOOL_NAME += pbpaste
pbpaste_FILES = pbpaste.m
pbpaste_FRAMEWORKS = UIKit
pbpaste_INSTALL_PATH = $(BINPATH)

TOOL_NAME += play
play_FILES = play.m
play_FRAMEWORKS = AVFoundation
play_INSTALL_PATH = $(BINPATH)

TOOL_NAME += post
post_FILES = post.c
post_FRAMEWORKS = CoreFoundation
post_INSTALL_PATH = $(BINPATH)

TOOL_NAME += record
record_FILES = record.m
record_FRAMEWORKS = AVFoundation CoreMedia CoreVideo QuartzCore
record_PRIVATE_FRAMEWORKS = $(capture_PRIVATE_FRAMEWORKS)
record_CODESIGN_FLAGS = -Srecord.xml
record_INSTALL_PATH = $(BINPATH)

TOOL_NAME += rmwifi
rmwifi_FILES = rmwifi.c
rmwifi_FRAMEWORKS = CoreFoundation
rmwifi_PRIVATE_FRAMEWORKS = MobileWiFi
rmwifi_CODESIGN_FLAGS = -Srmwifi.xml
rmwifi_INSTALL_PATH = $(BINPATH)

TOOL_NAME += say
say_FILES = say.m
say_PRIVATE_FRAMEWORKS = AVFoundation VoiceServices
say_INSTALL_PATH = $(BINPATH)

TOOL_NAME += socks
socks_FILES = socks.c
socks_FRAMEWORKS = CoreFoundation SystemConfiguration
socks_CODESIGN_FLAGS = -Ssocks.xml
socks_INSTALL_PATH = $(BINPATH)

TOOL_NAME += uncar
uncar_FILES = uncar.m
uncar_FRAMEWORKS = ImageIO MobileCoreServices
uncar_PRIVATE_FRAMEWORKS = CoreUI
uncar_INSTALL_PATH = $(BINPATH)

TOOL_NAME += wol
wol_FILES = wol.c
wol_INSTALL_PATH = $(BINPATH)

include $(THEOS_MAKE_PATH)/tool.mk
