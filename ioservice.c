#import <CoreFoundation/CoreFoundation.h>

typedef mach_port_t io_object_t;
typedef io_object_t io_iterator_t;
typedef io_object_t io_service_t;

extern mach_port_t kIOMasterPortDefault;
extern CFMutableDictionaryRef IOServiceMatching(const char*);
extern kern_return_t IOServiceGetMatchingServices(mach_port_t,CFDictionaryRef,io_iterator_t*);
extern kern_return_t IORegistryEntryCreateCFProperties(io_service_t,CFDictionaryRef*,CFAllocatorRef,UInt32);
extern io_object_t IOIteratorNext(io_iterator_t iterator);
extern CFStringRef IOObjectCopyClass(io_object_t);
extern kern_return_t IOObjectRelease(io_object_t);

static void printIndent(CFIndex level,FILE* fh) {
  for (CFIndex i=0;i<level;i++){fputs("  ",fh);}
}
static void printString(CFStringRef string,FILE* fh) {
  fputc('"',fh);
  const CFIndex length=CFStringGetLength(string);
  CFStringInlineBuffer buffer;
  CFStringInitInlineBuffer(string,&buffer,CFRangeMake(0,length));
  for (CFIndex i=0;i<length;i++){
    UniChar c=CFStringGetCharacterFromInlineBuffer(&buffer,i);
    const char* escaped="\a\b\e\f\n\r\t\v\\\"";
    const char* ematch=strchr(escaped,c);
    if(ematch){
      fputc('\\',fh);
      fputc(("abefnrtv\\\"")[ematch-escaped],fh);
    }
    else if(c>0xff){fprintf(fh,"\\u%04X",c);}
    else if(c<0x20 || c>=0x7f){fprintf(fh,"\\x%02X",c);}
    else {fputc(c,fh);}
  }
  fputc('"',fh);
}
static int compareKeys(const void* ptr1,const void* ptr2) {
  const CFTypeRef key1=*(CFTypeRef*)ptr1,key2=*(CFTypeRef*)ptr2;
  const CFTypeID stringType=CFStringGetTypeID();
  return (CFGetTypeID(key1)==stringType)?
   (CFGetTypeID(key2)==stringType)?CFStringCompare(key1,key2,0):-1:
   (CFGetTypeID(key2)==stringType)?1:0;
}
static void printDescription(CFTypeRef object,CFIndex level,FILE* fh) {
  CFTypeID typeID=CFGetTypeID(object);
  if(typeID==CFArrayGetTypeID()){
    fputc('[',fh);
    const CFIndex count=CFArrayGetCount(object);
    CFTypeRef* values=malloc(count*sizeof(*values));
    CFArrayGetValues(object,CFRangeMake(0,count),values);
    for (CFIndex i=0;i<count;i++){
      if(i){fputc(',',fh);}
      printDescription(values[i],level,fh);
    }
    free(values);
    fputc(']',fh);
  }
  else if(typeID==CFDictionaryGetTypeID()){
    fputc('{',fh);
    const CFIndex count=CFDictionaryGetCount(object);
    if(count){
      ++level;
      CFTypeRef* list=malloc(count*2*sizeof(*list));
      CFTypeRef* values=malloc(count*sizeof(*values));
      CFDictionaryGetKeysAndValues(object,list+count,values);
      for (CFIndex i=0;i<count;i++){
        list[2*i]=list[i+count];
        list[2*i+1]=values[i];
      }
      free(values);
      qsort(list,count,2*sizeof(*list),compareKeys);
      for (CFIndex i=0;i<count;i++){
        if(i){fputc(',',fh);}
        fputc('\n',fh);
        printIndent(level,fh);
        printDescription(list[2*i],level,fh);
        fputs(": ",fh);
        printDescription(list[2*i+1],level,fh);
      }
      free(list);
      fputc('\n',fh);
      printIndent(--level,fh);
    }
    fputc('}',fh);
  }
  else if(typeID==CFDataGetTypeID()){
    fputc('"',fh);
    const CFIndex length=CFDataGetLength(object);
    const UInt8* bytes=CFDataGetBytePtr(object);
    for (CFIndex i=0;i<length;i++){fprintf(fh,"%02X",bytes[i]);}
    fputc('"',fh);
  }
  else if(typeID==CFStringGetTypeID()){
    printString(object,fh);
  }
  else if(typeID==CFBooleanGetTypeID()){
    fputs(CFBooleanGetValue(object)?"true":"false",fh);
  }
  else if(typeID==CFNumberGetTypeID()){
    if(CFNumberIsFloatType(object)){
      double value;
      if(CFNumberGetValue(object,kCFNumberDoubleType,&value)){fprintf(fh,"%.15g",value);}
      else {fputs("-0.0",fh);}
    }
    else {
      long long value;
      if(CFNumberGetValue(object,kCFNumberLongLongType,&value)){fprintf(fh,"%lld",value);}
      else {fputs("-0",fh);}
    }
  }
  else {
    CFStringRef description=CFCopyDescription(object);
    printString(description,fh);
    CFRelease(description);
  }
}

int main(int argc,char** argv) {
  if(argc<2){
    fprintf(stderr,"Usage: %s <class>\n",argv[0]);
    return 1;
  }
  io_iterator_t iterator;
  kern_return_t status=IOServiceGetMatchingServices(kIOMasterPortDefault,
   IOServiceMatching(argv[1]),&iterator);
  if(status){printf("<error:0x%x>\n",status);}
  else {
    io_service_t service;
    while((service=IOIteratorNext(iterator))){
      CFStringRef name=IOObjectCopyClass(service);
      printString(name,stdout);
      fputs(": ",stdout);
      CFDictionaryRef dict;
      kern_return_t status=IORegistryEntryCreateCFProperties(service,&dict,NULL,0);
      if(status){printf("<error:0x%x>\n",status);}
      else {
        printDescription(dict,0,stdout);
        fputc('\n',stdout);
        CFRelease(dict);
      }
      IOObjectRelease(service);
    }
    IOObjectRelease(iterator);
  }
}
