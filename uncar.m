#import <ImageIO/ImageIO.h>
#import <MobileCoreServices/MobileCoreServices.h>
#include <objc/runtime.h>

@interface CUINamedImage
-(NSInteger)idiom;
-(CGImageRef)image;
-(CGFloat)scale;
-(NSUInteger)subtype;
@end

@interface CUIStructuredThemeStore
-(NSArray*)allImageNames;
-(NSArray*)imagesWithName:(NSString*)name;
@end

@interface CUIThemeFacet
+(CUIStructuredThemeStore*)_themeStoreForThemeIndex:(NSUInteger)theme;
+(NSUInteger)themeWithContentsOfURL:(NSURL*)URL error:(NSError**)error;
@end

int main(int argc,char** argv) {@autoreleasepool {
  if(argc<2){
    fprintf(stderr,"Usage: %s <assets.car>\n",argv[0]);
    return 1;
  }
  Class $CUIThemeFacet=objc_getClass("CUIThemeFacet");
  NSError* error=nil;
  NSUInteger theme=[$CUIThemeFacet themeWithContentsOfURL:
   [NSURL fileURLWithFileSystemRepresentation:argv[1]
   isDirectory:NO relativeToURL:nil] error:&error];
  if(error){
    fprintf(stderr,"E[theme]: %s\n",error.localizedDescription.UTF8String);
    return 1;
  }
  const CFStringRef UTI=kUTTypePNG;
  CFStringRef ext=UTTypeCopyPreferredTagWithClass(UTI,kUTTagClassFilenameExtension);
  CUIStructuredThemeStore* store=[$CUIThemeFacet _themeStoreForThemeIndex:theme];
  unsigned long count=0;
  for (NSString* name in store.allImageNames){
    for (CUINamedImage* image in [store imagesWithName:name]){
      CGImageRef cgImage=image.image;
      if(!cgImage){continue;}
      NSString* fname=[[name stringByAppendingFormat:@"@%.0fx~%ld,%lu",
       image.scale,(long)image.idiom,(unsigned long)image.subtype]
       stringByAppendingPathExtension:(NSString*)ext];
      CGImageDestinationRef dest=CGImageDestinationCreateWithURL(
       (CFURLRef)[NSURL fileURLWithPath:fname isDirectory:NO],UTI,1,NULL);
      CGImageDestinationAddImage(dest,cgImage,NULL);
      if(CGImageDestinationFinalize(dest)){count++;}
      CFRelease(dest);
    }
  }
  CFRelease(ext);
  printf("%ld images written.\n",count);
}}
