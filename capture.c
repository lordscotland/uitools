#import <CoreVideo/CVPixelBuffer.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <ImageIO/CGImageDestination.h>
#include "capture.h"

int main() {
  IOReturn status;
  IOMobileFramebufferRef iomfb;
  if((status=IOMobileFramebufferGetMainDisplay(&iomfb))){
    fprintf(stderr,"E[IOMobileFramebufferGetMainDisplay]: %d\n",status);
    return 1;
  }
  CGSize size;
  if((status=IOMobileFramebufferGetDisplaySize(iomfb,&size))){
    fprintf(stderr,"E[IOMobileFramebufferGetDisplaySize]: %d\n",status);
    return 1;
  }
  const int W=size.width,H=size.height;
  const int bpe=4,bpr=bpe*W,btot=bpr*H;
  CFTypeRef values[]={CFNumberCreate(NULL,kCFNumberIntType,(int[]){kCVPixelFormatType_32BGRA}),
   CFNumberCreate(NULL,kCFNumberIntType,&W),CFNumberCreate(NULL,kCFNumberIntType,&H),
   CFNumberCreate(NULL,kCFNumberIntType,&bpe),CFNumberCreate(NULL,kCFNumberIntType,&bpr),
   CFNumberCreate(NULL,kCFNumberIntType,&btot),kCFBooleanTrue};
  CFDictionaryRef attrs=CFDictionaryCreate(NULL,
   (CFTypeRef[]){kIOSurfacePixelFormat,kIOSurfaceWidth,kIOSurfaceHeight,
   kIOSurfaceBytesPerElement,kIOSurfaceBytesPerRow,kIOSurfaceAllocSize,kIOSurfaceIsGlobal},
   values,7,NULL,NULL);
  IOSurfaceRef surface=IOSurfaceCreate(attrs);
  CFRelease(attrs);
  int i;for (i=0;i<6;i++){CFRelease(values[i]);}
  IOSurfaceLock(surface,0,NULL);
  CARenderServerRenderDisplay(0,CFSTR("LCD"),surface,0,0);
  CGDataProviderRef provider=CGDataProviderCreateWithData(
   NULL,IOSurfaceGetBaseAddress(surface),btot,NULL);
  CGColorSpaceRef cspace=CGColorSpaceCreateDeviceRGB();
  CGImageRef img=CGImageCreate(W,H,8,8*bpe,bpr,cspace,
   kCGImageAlphaNoneSkipFirst|kCGBitmapByteOrder32Little,
   provider,NULL,true,kCGRenderingIntentDefault);
  CFRelease(cspace);
  CFRelease(provider);
  CFMutableDataRef data=NULL;
  CGImageDestinationRef dest;
  if(isatty(fileno(stdout))){
    const char* fn="screen.png";
    CFURLRef URL=CFURLCreateFromFileSystemRepresentation(
     NULL,(const UInt8*)fn,strlen(fn),false);
    dest=CGImageDestinationCreateWithURL(URL,kUTTypePNG,1,NULL);
    CFRelease(URL);
  }
  else {
    data=CFDataCreateMutable(NULL,0);
    dest=CGImageDestinationCreateWithData(data,kUTTypePNG,1,NULL);
  }
  CGImageDestinationAddImage(dest,img,NULL);
  CGImageDestinationFinalize(dest);
  IOSurfaceUnlock(surface,0,NULL);
  CFRelease(dest);
  CFRelease(img);
  CFRelease(surface);
  if(data){
    fwrite(CFDataGetBytePtr(data),sizeof(UInt8),CFDataGetLength(data),stdout);
    CFRelease(data);
  }
}
