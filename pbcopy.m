int main(int argc,char** argv) {@autoreleasepool {
  NSFileHandle* input=[NSFileHandle fileHandleWithStandardInput];
  NSData* data=[input readDataToEndOfFile];
  if(isatty([input fileDescriptor])){
    NSUInteger end=[data length];
    if(end){
      const char* ptr=(const char*)[data bytes];
      if(ptr[--end]=='\n'){data=[data subdataWithRange:NSMakeRange(0,end)];}
    }
  }
  [[UIPasteboard generalPasteboard] setData:data
   forPasteboardType:@"public.utf8-plain-text"];
}}
