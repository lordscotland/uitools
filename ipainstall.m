#include <fcntl.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include "miniz.h"

@interface LSApplicationWorkspace
+(instancetype)defaultWorkspace;
-(BOOL)installApplication:(NSURL*)path withOptions:(NSDictionary*)options error:(NSError**)error;
@end

int main(int argc,char** argv) {@autoreleasepool {
  if(argc<2){
    fprintf(stderr,"Usage: %s <file.ipa>\n",argv[0]);
    return 1;
  }
  const char* path=argv[1];
  NSDictionary* options=nil;
  int fd=open(path,O_RDONLY);
  if(fd==-1){return -1;}
  do {
    struct stat stbuf;
    if(fstat(fd,&stbuf)){break;}
    void* mem=mmap(NULL,stbuf.st_size,PROT_READ,MAP_PRIVATE,fd,0);
    if(mem==MAP_FAILED){break;}
    mz_zip_archive zip;
    if(mz_zip_reader_init_mem(&zip,mem,stbuf.st_size,MZ_ZIP_FLAG_DO_NOT_SORT_CENTRAL_DIRECTORY)){
      const mz_uint nfiles=mz_zip_reader_get_num_files(&zip);
      for (mz_uint i=0;i<nfiles;i++){
        char buf[1<<16];
        mz_zip_reader_get_filename(&zip,i,buf,sizeof(buf));
        char* ptr;
        if(!(ptr=strtok(buf,"/")) || strcmp(ptr,"Payload") || !strtok(NULL,"/")
         || !(ptr=strtok(NULL,"")) || strcmp(ptr,"Info.plist")){continue;}
        UInt8* bytes;
        size_t size;
        if((bytes=mz_zip_reader_extract_to_heap(&zip,i,&size,0))){
          CFReadStreamRef stream=CFReadStreamCreateWithBytesNoCopy(NULL,bytes,size,kCFAllocatorNull);
          if(stream){
            CFReadStreamOpen(stream);
            CFPropertyListRef plist=CFPropertyListCreateWithStream(NULL,stream,0,0,NULL,NULL);
            CFReadStreamClose(stream);
            CFRelease(stream);
            if(plist){
              if(CFGetTypeID(plist)==CFDictionaryGetTypeID()){
                NSString* string=CFDictionaryGetValue(plist,kCFBundleIdentifierKey);
                if(string){
                  options=@{(id)kCFBundleIdentifierKey:string};
                  printf("BundleIdentifier: %s\n",string.UTF8String);
                  if((string=CFDictionaryGetValue(plist,kCFBundleNameKey))){
                    printf("BundleName: %s\n",string.UTF8String);
                  }
                  if((string=CFDictionaryGetValue(plist,kCFBundleVersionKey))){
                    printf("BundleVersion: %s\n",string.UTF8String);
                  }
                  if((string=CFDictionaryGetValue(plist,CFSTR("MinimumOSVersion")))){
                    printf("MinimumOSVersion: %s\n",string.UTF8String);
                  }
                }
              }
              CFRelease(plist);
            }
          }
          mz_free(bytes);
          if(options){break;}
        }
      }
      mz_zip_reader_end(&zip);
    }
    munmap(mem,stbuf.st_size);
  } while(0);
  close(fd);
  CFURLRef URL=CFURLCreateFromFileSystemRepresentation(NULL,
   (const UInt8*)path,strlen(path),false);
  NSError* error;
  BOOL success=[[LSApplicationWorkspace defaultWorkspace]
   installApplication:(NSURL*)URL withOptions:options error:&error];
  CFRelease(URL);
  if(success){fputs("Installation finished.\n",stdout);}
  else {printf("Error: %s.\n",error.localizedDescription.UTF8String);}
  return success?0:-1;
}}
