#import <AVFoundation/AVFoundation.h>

@interface AudioPlayerDelegate : NSObject @end
@implementation AudioPlayerDelegate
+(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer*)player error:(NSError*)error {
  fprintf(stderr,"E[AudioPlayerDelegate]: %s\n",error.localizedDescription.UTF8String);
}
+(void)audioPlayerDidFinishPlaying:(AVAudioPlayer*)player successfully:(BOOL)success {
  CFRunLoopStop(CFRunLoopGetMain());
}
@end

int main(int argc,char** argv) {
  enum {
    kFlag_nloops=1,
    kFlag_gain=2,
    kFlag_pan=4,
  } flags=0;
  int p_nloops;
  float p_gain,p_pan;
  int opt;
  while((opt=getopt(argc,argv,"n:g:p:"))!=-1){
    if(!optarg){return 1;}
    switch(opt){
      case 'n':flags|=kFlag_nloops;
        if(sscanf(optarg,"%d",&p_nloops)==1){continue;}
        break;
      case 'g':flags|=kFlag_gain;
        if(sscanf(optarg,"%f",&p_gain)==1 && p_gain>=0 && p_gain<=1){continue;}
        break;
      case 'p':flags|=kFlag_pan;
        if(sscanf(optarg,"%f",&p_pan)==1 && p_pan>=0 && p_pan<=1){continue;}
        break;
      default:
        fprintf(stderr,"W: Ignoring option -%c\n",opt);
        continue;
    }
    fprintf(stderr,"E[-%c]: Invalid argument\n",opt);
    return 1;
  }
  if(optind>=argc){
    fprintf(stderr,"Usage: %s [-n <nloops>] [-g <gain(0..1)>] [-p <pan(-1..1)>] <file>\n",argv[0]);
    return 1;
  }
  NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
  AVAudioSession* session=[AVAudioSession sharedInstance];
  NSError* error;
  if(![session setCategory:AVAudioSessionCategoryPlayback error:&error]){
    fprintf(stderr,"E[AVAudioSession]: %s\n",error.localizedDescription.UTF8String);
  }
  if(![session setActive:YES error:&error]){
    fprintf(stderr,"E[AVAudioSession]: %s\n",error.localizedDescription.UTF8String);
  }
  for (opt=optind;opt<argc;opt++){
    char* infn=argv[opt];
    NSURL* URL=[NSURL fileURLWithPath:[[NSFileManager defaultManager]
     stringWithFileSystemRepresentation:infn length:strlen(infn)]];
    printf("Playing [%s]...\n",URL.path.UTF8String);
    AVAudioPlayer* player=[[AVAudioPlayer alloc] initWithContentsOfURL:URL error:&error];
    if(!player){
      fprintf(stderr,"E[AVAudioPlayer]: %s\n",error.localizedDescription.UTF8String);
      continue;
    }
    player.delegate=(id)[AudioPlayerDelegate class];
    if(flags&kFlag_nloops){player.numberOfLoops=p_nloops;}
    if(flags&kFlag_gain){player.volume=p_gain;}
    if(flags&kFlag_pan){player.pan=p_pan;}
    [player play];
    CFRunLoopRun();
    [player stop];
    [player release];
  }
  [pool drain];
}
