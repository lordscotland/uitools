#import <AVFoundation/AVFoundation.h>

@interface VSSpeechSynthesizer : NSObject
@property(assign) id delegate;
@property(assign) float rate,volume;
-(id)startSpeakingString:(NSString*)string;
-(id)startSpeakingString:(NSString*)string error:(NSError*)error;
-(id)startSpeakingString:(NSString*)string toURL:(CFURLRef)URL;
-(id)startSpeakingString:(NSString*)string toURL:(CFURLRef)URL error:(NSError*)error;
@end

@interface SpeechSynthesizerDelegate : NSObject @end
@implementation SpeechSynthesizerDelegate
+(void)speechSynthesizer:(VSSpeechSynthesizer*)synth didFinishSpeaking:(BOOL)finished withError:(NSError*)error {
  if(!finished){fprintf(stderr,"E: %s\n",error.localizedDescription.UTF8String);}
  CFRunLoopStop(CFRunLoopGetMain());
}
@end

int main(int argc,char** argv) {
  enum {
    kFlag_rate=1,
    kFlag_volume=2,
  } flags=0;
  char* outfn=NULL;
  float s_rate,s_volume;
  int opt;
  while((opt=getopt(argc,argv,"o:r:V:"))!=-1){
    if(!optarg){return 1;}
    switch(opt){
      case 'o':outfn=optarg;continue;
      case 'r':flags|=kFlag_rate;
        if(sscanf(optarg,"%f",&s_rate)==1){continue;}
        break;
      case 'V':flags|=kFlag_volume;
        if(sscanf(optarg,"%f",&s_volume)==1){continue;}
        break;
      default:
        fprintf(stderr,"W: Ignoring option -%c\n",opt);
        continue;
    }
    fprintf(stderr,"-%c: Invalid argument\n",opt);
    return 1;
  }
  if(optind>=argc && isatty(fileno(stdin))){
    fprintf(stderr,"Usage: %s [-o <output.caf>] [-r <rate>] [-V <volume>] [<string>]\n",argv[0]);
    return 1;
  }
  NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
  AVAudioSession* session=[AVAudioSession sharedInstance];
  NSError* error;
  if(![session setCategory:AVAudioSessionCategoryPlayback error:&error]){
    fprintf(stderr,"E[AVAudioSession]: %s\n",error.localizedDescription.UTF8String);
  }
  if(![session setActive:YES error:&error]){
    fprintf(stderr,"E[AVAudioSession]: %s\n",error.localizedDescription.UTF8String);
  }
  NSString* string=nil;
  if(optind!=argc-1 || strcmp(argv[optind],"-")!=0){
    NSMutableString* buffer=[NSMutableString string];
    for (opt=optind;opt<argc;opt++){[buffer appendFormat:@"%s ",argv[opt]];}
    if(buffer.length){string=buffer;}
  }
  if(!string){
    string=[[[NSString alloc] initWithData:[[NSFileHandle
     fileHandleWithStandardInput] readDataToEndOfFile]
     encoding:NSUTF8StringEncoding] autorelease];
  }
  VSSpeechSynthesizer* synth=[[VSSpeechSynthesizer alloc] init];
  synth.delegate=[SpeechSynthesizerDelegate class];
  if(flags&kFlag_rate){synth.rate=s_rate;}
  if(flags&kFlag_volume){synth.volume=s_volume;}
  if(outfn){
    CFURLRef URL=CFURLCreateFromFileSystemRepresentation(NULL,
     (const UInt8*)outfn,strlen(outfn),false);
    if([synth respondsToSelector:@selector(startSpeakingString:toURL:error:)]){
      [synth startSpeakingString:string toURL:URL error:NULL];
    }
    else {[synth startSpeakingString:string toURL:URL];}
    CFRelease(URL);
  }
  else if([synth respondsToSelector:@selector(startSpeakingString:error:)]){
    [synth startSpeakingString:string error:NULL];
  }
  else {[synth startSpeakingString:string];}
  CFRunLoopRun();
  [synth release];
  [pool drain];
}
