@interface MCProfileConnection
+(id)sharedConnection;
-(NSDictionary*)effectiveUserSettings;
-(void)setBoolValue:(BOOL)value forSetting:(NSString*)setting;
@end

int main(int argc,char** argv) {@autoreleasepool {
  MCProfileConnection* profile=[MCProfileConnection sharedConnection];
  NSDictionary* settings=[profile.effectiveUserSettings
   objectForKey:@"restrictedBool"];
  if(argc<2){
    for (NSString* key in [settings.allKeys
     sortedArrayUsingSelector:@selector(localizedStandardCompare:)]){
      NSNumber* value=[[settings objectForKey:key] objectForKey:@"value"];
      printf("%c %s\n",value?value.boolValue?'+':'-':' ',key.UTF8String);
    }
    return 0;
  }
  int i;
  for (i=1;i<argc;i++){
    const char* $key=argv[i];
    char c0=$key[0];
    if(c0=='+' || c0=='-' || c0==':'){$key++;}
    if(!$key[0]){
      fprintf(stderr,"E: Null key\n\nUsage: %s [+|-]<key> ...\n",argv[0]);
      return 1;
    }
    NSString* key=[NSString stringWithUTF8String:$key];
    if(![settings objectForKey:key]){
      fprintf(stderr,"W: Undefined key [%s]\n",$key);
      continue;
    }
    if(c0=='+' || c0=='-'){[profile setBoolValue:c0=='+' forSetting:key];}
    else {
      NSNumber* value=[[settings objectForKey:key] objectForKey:@"value"];
      c0=value?value.boolValue?'+':'-':' ';
    }
    printf("%c %s\n",c0,$key);
  }
}}
